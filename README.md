# I Love Vietnam Mode

Première application web développé afin d’apprendre le framework Symfony 4 durant ma formation de Développeur Web et Web Mobile.

Objectifs :
-	Familiarisation avec le framework Symfony 4
-	Création d’un outil d’administration des stocks et ventes de produits de mode
-	Enregistrement des produits commandés
-	Enregistrement de la date de réception
-	Visualisation des produits en stock
-	Création de catégories de produits
-	Enregistrement des ventes réalisé

Outils :
-	Symfony 4
-	ORM Doctrine
-	Bootstrap
-	jQuery

Branche lastWebsite pour visionner le travail réalisé.
